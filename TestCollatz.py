#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, find_cycle_length

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)

    def test_read_3(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_3(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_4(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_5(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_6(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    def test_eval_7(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 10, 1, 20)
        self.assertEqual(w.getvalue(), "10 1 20\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO(
            "1 10\n 10 1\n100 200\n201 210\n900 1000\n1000 900\n10 10\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n10 1 20\n100 200 125\n201 210 89\n900 1000 174\n1000 900 174\n10 10 7\n")

    def test_solve2(self):
        r = StringIO("1 34\n16 11\n15 36\n6 35\n125 100\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 34 112\n16 11 18\n15 36 112\n6 35 112\n125 100 114\n")

    def test_solve3(self):
        r = StringIO("1 1\n1 1000\n200 500\n20 5\n10000 100005\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n1 1000 179\n200 500 144\n20 5 21\n10000 100005 351\n")

    # -----
    # find_cycle_lenghs
    # -----
    def test_cycle_lengths_1(self):
        m = find_cycle_length(855)
        self.assertEqual(m, 55)

    def test_cycle_lengths_2(self):
        m = find_cycle_length(709)
        self.assertEqual(m, 34)

    def test_cycle_lengths_3(self):
        m = find_cycle_length(62)
        self.assertEqual(m, 108)

    def test_cycle_lengths_4(self):
        m = find_cycle_length(23)
        self.assertEqual(m, 16)

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
